# UniprotPPI

This project aims at retrieving the proteins that interact with a protein of interest from UniProt using SPARQL (e.g. for INSR_HUMAN ([P06213](https://www.uniprot.org/uniprotkb/P06213)) below).

Section 1 consists in securing a convenient access to the data that will be used in the next sections.
Section 2 then explores the data in order to determine the structure of the relevant part of the data model.
Section 3 relies on this structure for writing iteratively the SPARQL query.

![all the proteins that interact with INSR_HUMAN (P06213)](./data/P06213-interactions.png "Interactome for INSR_HUMAN (P06213)")


## 1. Access data

You can either

- Use the Uniprot SPARQL endpoint [https://sparql.uniprot.org/sparql/](https://sparql.uniprot.org/sparql/)
    - pros:
        - you do not have to set up an endpoint
        - always up to date
    - cons:
        - you do not learn to set up an endpoint
        - requires an internet connection
        - the dataset is so big that it makes its exploration cumbersome
- Or set up your own endpoint with the data about your protein of interest (e.g. P06213)
    - pros:
        - can work offline
        - facilitates the exploration of the dataset (somehow)
        - you get to learn to set up your own endpoint
    - cons:
        - requires some tinkering
    - do not even try to download uniprot :-)
    - retrieve the data either
        - from the web page, link "Download" at the top of the page
        - from the URL: add the "`.rdf`" suffix to the URL `http://uniprot.org/uniprot/P06213` and download [http://uniprot.org/uniprot/P06213.rdf](http://uniprot.org/uniprot/P06213.rdf)
    - a local copy of the file is available from the `data/` directory as `data/P06213.rdf`
    - convert the RDF file from the RDF/XML format to turtle (optional but recommended, it will make the reading easier): `rapper -i rdfxml -o turtle P06213.rdf > P06213.ttl`
    - a local copy of the file is available from the `data/` directory as `data/P06213.ttl`
    - with [Apache fuseki]()
        - see [https://gitlab.com/odameron/rdf-sparql-cheatsheet](https://gitlab.com/odameron/rdf-sparql-cheatsheet)
        - download `apache-jena-fuseki-X.Y.Z.tar.gz` from [https://jena.apache.org/download/](https://jena.apache.org/download/)
        - in a terminal:
            - (optional) declare a `FUSEKI_HOME` variable: `export FUSEKI_HOME=/path/to/apache-jena-fuseki-X.Y.Z/`
            - start fuseki (no trailing `&` and leave the terminal open): `${FUSEKI_HOME}/fuseki-server --file=/path/to/data/P06213.ttl /uniprot`
        - in a web browser, open [http://localhost:3030](http://localhost:3030)


## 2. Explore the data to figure out the data model

The goal of this section is to explore the dataset in order to elucidate how the interaction between INSR_HUMAN and IGF1_HUMAN is represented.
You are strongly encouraged to experiment and give it a try by yourself.
If you need so, the following steps will guide you throughout the process.

- from the figure above, INSR_HUMAN interacts with IGF1_HUMAN. We will explore the data to find how they are connected. Note that if you use the RDF description of INSR_HUMAN, you can expect to have many triples originating from INSR for representing everything UniProt knows about it.
Among them, probably few lead to IGF1.
Therefore, starting from IGF1 rather than from INSR seems the best heuristics.
- from the UniProt website, find the identifer of IGF1_HUMAN (should be something like P0501???)
- open `data/P06213.ttl` in a text editor
- look for "P0501???"
- what is its URI?
- you will find that it is associated to an entity EBI-790227???
- draw a diagram of the triples having EBI-790227??? as a subject
    - put EBI-790227??? at the center, and leave some space around it, we will extend the diagram later
    - the `a` property is a (stupid and ugly) shortcut for `rdf:type`
- interpretation: EBI-790227??? is an entity (the URI lets us guess that it comes from the "intact" protein-protein interaction database [https://www.ebi.ac.uk/intact/home](https://www.ebi.ac.uk/intact/home)) that is the same thing as uniprot:P0501??? and has a label "IGF1". So basically, the human gene IGF1 is represented by two entities:
    - uniprot:P0501??? in the uniprot database
    - intact:EBI-790227??? in the intact database
- at this point, we still haven't found how P0501??? is connected to INSR_HUMAN (P06213), but EBI-790227??? seems to be the next neighbor in the path
- look for "EBI-790227???" in the file
- you will find that it is associated to an entity 47589???-790227???
- complete your previous diagram with the triples having 47589???-790227??? as the subject
- interpretation: bingo, it looks like this entity represents an interaction (hints: the fact that it is an instance of `uniprot:Interaction` + the pattern of its identifier) that has two participants, one of which is the "intact" counterpart of IGF1
- notice the `uniprot:experiments` property that likely represents the number of experiments supporting the interaction between INSR and IGF1
- follow the link to the other participant of the interaction, and hopefuly that will lead you to P06213
- infer the path connecting P06213 (INSR where we started) to P0501??? (one of the proteins it interacts with), as well as the relevant intermediate informations
- **NB:** notice that since we want to add some informations characterizing the interaction beetween P06213 and P0501??? (for example the number of experiments that support this interaction), we cannot have a single triple connecting the two directly, so we used some intermediary entity (here 47589???-790227???) to represent the relation and the associated information. This is a typical example of _reification_.


**NB:** see the "Exploration with rdfCartographer" section below, there was actually a shorter path from `uniprot:P06213` to `uniprot:P05019` through the `up:interaction` property.
Our text-based search lead us to missing it.


## 3. Iteratively build the query

The goal of this section is to create a SPARQL query that retrieves all the proteins that interacts with INSR_HUMAN, as well as the number of experiments that support these interactions.
You are encouraged to proceed and experiment on your own (tip: start from the template below).

Your queries will elaborate on the following template:

```sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl:<http://www.w3.org/2002/07/owl#> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
PREFIX uniprot: <http://purl.uniprot.org/uniprot/>
PREFIX up:<http://purl.uniprot.org/core/>

SELECT ?proteinIntact 
WHERE {
  # VALUES allows to explicitely assign some value(s) to a variable
  # so in this query, ?protein will always be bound to uniprot:P06213
  # it is convient because
  # - there is a single place ot modify if we decide to look for another protein
  # - we just need to comment the line if we want the query to examine all the proteins
  VALUES ?protein { uniprot:P06213 }

  # ?protein .......
}
```


- make sure that INSR URI is correct, e.g. by displaying its `up:mnemonic` value (NB: `rdfs:label` would have been a classical property to use; unfortunately it is present in the UniProt SPARQL endpoint but not in `P06213.ttl`)
- retrieve INSR identifier in the Intact database
- retrieve the interactions in which it participates
- retrieve the number of experiments supporting each interaction
    - sort them from the interactions(s) having the most supporting experiments to those having the least
- for each interaction, retrieve the other protein (may require some additional filtering to avoid duplicates) and its UniProt identifier
    - focus on one interaction and observe its participants


## Todo / future extensions

- [ ] use a `CONSTRUCT` query to generate the interaction graph
- [ ] self-interacting proteins
- [ ] only consider human proteins
- [ ] extend the query so that it also takes into account the interactions between the proteins that interact with INSR. For example, the matrix shows that P85A_HUMAN interactc not only with INSR_HUMAN (first column), but also with GRB10_HUMAN, IRS1_RAT and IRS1_HUMAN.
- [ ] visualize the graph of interactions
- [x] use-case for [rdfCartographer](https://gitlab.com/odameron/rdfCartographer)?
- [ ] the [uniprot page for INSR_HUMAN P06213](https://www.uniprot.org/uniprotkb/P06213/entry#interaction) seems to also consider its isoforms P06213-1 and P06213-2 (see dropdown lists)


## Exploration with rdfCartographer


```python
import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('https://sparql.uniprot.org/sparql/')
cartographer.add_prefix("uniprot", "http://purl.uniprot.org/uniprot/")
cartographer.add_prefix("intact", "http://purl.uniprot.org/intact/")
cartographer.add_prefix("up", "http://purl.uniprot.org/core/")

cartographer.show_neighbors("uniprot:P06213", relationIdent="owl:sameAs", relationDirection="Both")
# displays:
# http://purl.uniprot.org/intact/EBI-475899 --owl:sameAs--> uniprot:P06213
#
# so we need to follow owl:sameAs that points to P06213 (inbound)

# retrieve the Intact proteins for P06213 and P05019
cartographer.add_neighbors("uniprot:P06213", relationIdent="owl:sameAs", relationDirection = "Inbound")
cartographer.add_neighbors("uniprot:P05019", relationIdent="owl:sameAs", relationDirection = "Inbound")
# displays
#
# http://purl.uniprot.org/intact/EBI-475899  --owl:sameAs--> uniprot:P06213
# http://purl.uniprot.org/intact/EBI-7902275 --owl:sameAs--> uniprot:P05019

cartographer.show_neighbors("intact:EBI-475899")
# displays:
# 
# intact:EBI-475899 --rdf:type--> http://purl.uniprot.org/core/Participant
# intact:EBI-475899 --owl:sameAs--> http://purl.uniprot.org/uniprot/P06213
# intact:EBI-475899 --rdfs:label--> INSR
# http://purl.uniprot.org/intact/475899-7902275 --up:participant--> intact:EBI-475899
# ...

cartographer.show_neighbors("intact:475899-7902275")
# displays:
# 
# intact:475899-7902275	--rdf:type-->	up:Interaction
# intact:475899-7902275	--rdf:type-->	up:Non_Self_Interaction
# intact:475899-7902275	--up:experiments-->	4
# intact:475899-7902275	--up:participant-->	intact:EBI-475899
# intact:475899-7902275	--up:participant-->	intact:EBI-7902275
# intact:475899-7902275	--up:xeno-->	false
# uniprot:P05019	--up:interaction-->	intact:475899-7902275
# uniprot:P06213	--up:interaction-->	intact:475899-7902275

cartographer.add_neighbors("intact:475899-7902275")

cartographer.save_RDF_graph("uniprot-INSR-SubGraph.ttl")
```

![RDF representation of the interaction between INSR_HUMAN (P06213) and IGF1_HUMAN (P05019)](./data/uniprot-INSR-SubGraph.png "Interaction between INSR_HUMAN (P06213) and IGF1_HUMAN (P05019)")
